from driehoeken.lijnen import lijnen, op_lijn2, op_lijn3

def sorted(a, b, c):

    grootste = max(max(a, b), c)
    kleinste = min((min(a, b), c))
    middelste = list({a, b, c} - {grootste, kleinste})[0]

    return f'{kleinste}-{middelste}-{grootste}'


oplossingen = set()

c1 = 0
c2 = 0

for a3 in range(1, 12):
    for a2 in range(1, 12):
        for a1 in range(1, 12):

            if (a1 == a2) or (a1 == a3) or (a2 == a3):
                continue

            c1 += 1
            if op_lijn2(a1, a2, lijnen) and op_lijn2(a1, a3, lijnen) and op_lijn2(a2, a3, lijnen) and not op_lijn3(a1, a2, a3, lijnen):

                c2 += 1
                oplossingen.add(sorted(a1, a2, a3))

s = ''
i = 0
for oplossing in oplossingen:
    i += 1
    s += f'{oplossing:>12}'
    if i%3 ==0:
        print(s)
        s = ''

print(len(oplossingen))

print(c1, c2)
