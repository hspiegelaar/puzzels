lijnen = [
    [1, 2, 6],
    [1, 3, 7, 9],
    [1, 4, 8, 10],
    [1, 5, 11],
    [2, 3, 4, 5],
    [6, 7, 8, 5],
    [6, 9, 10, 11]
]


def op_lijn2(p1, p2, lijnen):
    '''
    test of twee punten op een lijn liggen
    '''

    for lijn in lijnen:
        if p1 in lijn and p2 in lijn:
            return True

    return False

def op_lijn3(p1, p2, p3, lijnen):
    '''
    test of drie punten op een lijn liggen
    '''

    for lijn in lijnen:
        if p1 in lijn and p2 in lijn and p3 in lijn:
            return True

    return False

if __name__ == '__main__':

    assert op_lijn2(2, 4, lijnen)
    assert op_lijn2(9, 7, lijnen)
    assert op_lijn2(4, 10, lijnen)
    assert op_lijn2(1, 11, lijnen)
    assert not op_lijn2(7, 10, lijnen)

    assert op_lijn3(6, 7, 8, lijnen)
    assert op_lijn3(3, 4, 5, lijnen)
    assert op_lijn3(8, 7, 5, lijnen)
    assert not op_lijn3(2, 1, 10, lijnen)
